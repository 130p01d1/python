The World Wide Web does not contain a complete source with all the answers to the questions in the book 'How to Think Like a Computer Scientist' (the world's best book for learning 'how to think' and Python).

I will therefore myself publish all the answers here as soon as possible.

(All current other sources contains solutions to an outdated version of the book).

Link to book:
https://openbookproject.net/thinkcs/python/english3e/
