# This is a dictionary, dictionary has no order therefore key:value pairs can be listed in any order"
 
inventory = {"apples": 430, "bananas": 312, "oranges": 525, "pears": 217}

for k in inventory:   # The order of the k's is not defined
    print("Got key", k, "which maps to value", inventory[k])

ks = list(inventory.keys())
print(ks)