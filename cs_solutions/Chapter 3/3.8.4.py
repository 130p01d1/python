# 4. Suppose our turtle tess is at heading 0 — facing east. We execute the statement tess.left(3645). What does tess do, and what is her final heading?

left = 3645

print('tess spins', left // 360, 'complete turns and', left % 360, 'degrees.')



