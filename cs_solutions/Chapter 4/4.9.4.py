#!/usr/bin/python3

# 4. Draw this pretty pattern.

import turtle

def draw_square(t, sz):
    """Draw a square with turtle 't' and size 'sz'."""
    for i in range(4):
        t.forward(sz)
        t.left(90)

window = turtle.Screen()    #Set up the window and its attributes
window.bgcolor("lightgreen")
window.title("Pretty pattern")

tess = turtle.Turtle()      #Create tess and set some attributes
tess.color("blue")
tess.pensize(3)

for i in range(20):
    draw_square(tess, 100)
    tess.left(18)


window.mainloop()

"""
This took a while to figure out because I counted only five (5) squares.
It is in fact twenty (20) squares: 360 / 20 = 18 degrees.
I.e tilt turtle 'tess' 18 degrees after every square drawed.
"""