# 3. Write a void function draw_poly(t, n, sz) which makes a turtle draw a regular polygon. When called with draw_poly(tess, 8, 50), it will draw a shape like this:

import turtle

# Setup screen
w = turtle.Screen()
w.bgcolor("lightgreen")

# Create turtle
tess = turtle.Turtle()
tess.color("hot pink")
tess.pensize(3)


def draw_poly(t, n, sz):            # Define (create) function named 'draw_poly' with parameters 't', 'n' and 'sz'.
    """Draw a polygon with turtle 't', number of angles 'n' and size 'sz'."""
    for i in range(n):
        t.forward(sz)
        t.left(45)

draw_poly(tess, 8, 50)              # Call (use) the function 'draw_poly' with arguments 'tess', '8' and '50'.

w.mainloop()