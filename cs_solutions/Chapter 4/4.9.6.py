# Write a void function draw_equitriangle(t, sz) which calls draw_poly from the previous question to have its turtle draw a equilateral triangle.

import turtle

# Setup screen
w = turtle.Screen()
w.bgcolor("lightgreen")

# Create turtle
tess = turtle.Turtle()
tess.color("hot pink")
tess.pensize(3)


def draw_poly(t, n, sz):
    """Draw a polygon with turtle 't', number of angles 'n' and size 'sz'."""
    for i in range(n):
        t.forward(sz)
        t.left(120)

def draw_equitriangle(t, sz):
    """Draw a equilateral triangle."""
    draw_poly(t, 3, sz)

draw_equitriangle(tess, 200)

w.mainloop()

# 180 - 60 (degrees per angle in a equilateral triangle) = 120 degrees.