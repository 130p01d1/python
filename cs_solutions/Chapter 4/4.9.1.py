#!/usr/bin/python3

# 1 Write a void (non-fruitful) function to draw a square. Use it in a program to draw the image shown below. Assume each side is 20 units. (Hint: notice that the turtle has already moved away from the ending point of the last square when the program ends.)

import turtle

def draw_square(turtle, size):  # Define (create) a function named 'draw_square' with the parameters 'turtle' and 'size'.
    """Make 'turtle' draw a square of 'size'."""
    for i in range(4):
        turtle.forward(size)
        turtle.left(90)

window = turtle.Screen()        # Set up the window and its attributes.
window.bgcolor("lightgreen")

anna = turtle.Turtle()          # Create turtle named anna  and set some attributes.
anna.color("hot pink")
anna.pensize(3)

for i in range(5):              # Use a for loop to use the function 'draw_square' five times.
    draw_square(anna, 20)       # Call (use) the function 'draw_square' with the arguments 'anna' and '20'. Arguments attach to the respective parameters local variables in the called function. 
    anna.penup()
    anna.forward(40)            # Set position of new square
    anna.pendown()

window.mainloop()

# Note to self: A function is created by defining it 'def' and if needed setting parameters. A function is used by calling i.e. writing out is name and adding arguments if needed.