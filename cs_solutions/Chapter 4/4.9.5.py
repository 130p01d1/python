# 5. The two spirals in this picture differ only by the turn angle. Draw both.

import turtle

window = turtle.Screen()        # Set up the window and its attributes.
window.bgcolor("lightgreen")

tess = turtle.Turtle()          # Create tess and set some attributes.
tess.color("blue")
tess.pensize(3)
tess.speed(0)
tess.penup()
square_size = 5

def draw_square(t, sz):
    """Draw a square with turtle 't' and size 'sz'."""
    for i in range(2):
        t.forward(sz)
        t.pendown()
        t.right(90) 

for i in range(48):
    draw_square(tess, square_size)
    square_size = square_size + 8

window.mainloop()

# To get second spiral example change t.right(90) to (89).