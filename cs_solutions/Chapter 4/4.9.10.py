"""
Extend your program above. Draw five stars, but between each, pick up the pen, move forward by 350 units, turn right by 144, put the pen down, and draw the next star. You’ll get something like this:
"""

import turtle

size  = int(input("What size do you want on your star? "))

########################
# Functions
########################

def make_window(color, title):
    """
      Set up the window with the given background color and title.
      Returns the new window.
    """
    w = turtle.Screen()
    w.bgcolor(color)
    w.title(title)
    return w

def make_turtle(color, size):
    """
      Set up a turtle with the given color and pensize.
      Returns the new turtle.
    """
    t = turtle.Turtle()
    t.color(color)
    t.pensize(size)
    return t

def draw_stars(size):
    """
      Function to draw a five point star in various 'size'.
    """
    for i in range(5):
        for i in range(5):
            tess.forward(size)
            tess.left(-144)
        tess.penup()
        tess.forward(350)
        tess.right(144)
        tess.pendown()
    return i


wn = make_window("lightgreen", "Draw five pink stars")
tess = make_turtle("hotpink", 3)
stars = draw_stars(size)