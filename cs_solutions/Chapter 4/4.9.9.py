"""
9. Write a void function to draw a star, where the length of each side is 100 units.
(Hint: You should turn the turtle by 144 degrees at each point.)
"""

import turtle

size  = int(input("What size do you want on your star? "))

# set up screen
window = turtle.Screen()
window.bgcolor("white")

# create tess
tess = turtle.Turtle()
tess.color("black")
tess.pensize(3)
tess.speed(4)
tess.hideturtle()

def draw_star(sz):
    """Function to draw a five point star in various 'size'."""
    for i in range(5):
        tess.forward(sz)
        tess.left(-144)

draw_star(size)


window.mainloop()

# Remember to prompt for user input before you set up screen and create tess