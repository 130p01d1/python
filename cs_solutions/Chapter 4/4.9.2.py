# 2. Write a program to draw this. Assume the innermost square is 20 units per side, and each successive square is 20 units bigger, per side, than the one inside it.

import turtle

def draw_square(turtle, size):
    for i in range(4):
        turtle.forward(size)
        turtle.left(90)

window = turtle.Screen()
window.bgcolor("lightgreen")

anna = turtle.Turtle()
anna.color("hot pink")
anna.pensize(3)

square_size = 20

for i in range(5):
    draw_square(anna, square_size)
    anna.penup()
    anna.forward(-10)
    anna.right(90)
    anna.forward(10)
    anna.left(90)
    anna.pendown()
    square_size = square_size + 20

window.mainloop()