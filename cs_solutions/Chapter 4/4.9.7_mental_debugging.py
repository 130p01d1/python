def sum_to(n):
    counter = 0
    print("loop start")
    for i in range(1, n+1):
        print(f"counter = {counter} i = {i}")
        print(f"counter + i = {counter} + {i}")
        counter = counter + i
        print(f"counter är nu {counter}")
    print(counter)
    print("loop end")

sum_to(10)

# Use this 'mental debugging' to better understand what happens inside variable 'counter' and 'i'.