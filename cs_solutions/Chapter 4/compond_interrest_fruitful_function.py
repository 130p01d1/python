# creating fruitful function for compound interest 
def final_amt(principalAmount, nominalPercentageRate, numTimesPerYear, years):
    """Apply the compound interest formula to p to produce the final amount."""
    a = principalAmount * (1 + nominalPercentageRate / numTimesPerYear) ** (numTimesPerYear*years)
    return a

# calling function
toInvest = float(input("How much do you want to invest?"))
fnl = final_amt(toInvest, 0.08, 12, 5)
print("At the end of the period you'll have", fnl)