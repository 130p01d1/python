z = [1,2,3,4,5,6]
sum = 0
for i in z:
	sum += i
print("Sum : "+str(sum))

x = [
	{"Data": 1, "Sigh" : "Pust"},
	{"Data": 2, "Sigh" : "Pez"}
]

print(x[1]["Sigh"])

print(x[0].items())

for item in x:
	print(item)

for item in x:
	for key,data in item.items():
		print(key+":"+str(data))

p = [(1,2,3),(5,6,7)]

print(p[1])

for i in p:
	print(i)

for x,y,z in p:
	print(x,y,z)