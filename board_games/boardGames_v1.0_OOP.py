#!/usr/bin/python3

import os.path
import pickle
import sys
import readline

""" Class Game, stores a data about a single game"""
class Game:
	def __init__(self):
		pass

	def setData(self, gameData):
		self.gameData = gameData


	def printData(self):
		for key, data in self.gameData.items():
			print(key, " : ", data)


	def getRating(self):
		return int(self.gameData["Rating"])


""" Class BoardGames, manages games"""
class BoardGames:
	gameInfo = {
		"Title": "string",
		"NumberOfPlayers": "number",
		"GameTime": "number",
		"RecommendedAge": "number",
		"Rating": "number"
	}

	fileName = "gamesdata.dat"


	def __init__(self):
		self.gamesData = []
	

	# This function is from https://stackoverflow.com/questions/5403138/how-to-set-a-default-editable-string-for-raw-input/20351345
	def rlinput(self,prompt, prefill=''): 
		readline.set_startup_hook(lambda: readline.insert_text(prefill))
		try:
			return input(prompt)
		finally:
			readline.set_startup_hook()


	def loadGamesData(self):
		if os.path.isfile(self.fileName):
			file = open(self.fileName, 'rb')
			self.gamesData = pickle.load(file)
			file.close()
		else:
			print("File '"+self.fileName+"' not found. This is normal when you start the program for the first time and has not entered any data.")


	def saveGameData(self):
		file = open(self.fileName, 'wb')
		pickle.dump(self.gamesData, file)
		file.close()


	def listGameData(self):
		gameNumber = 1
		for game in self.gamesData:
			print("Game #", gameNumber)
			game.printData()
			gameNumber += 1
			print("--------------")


	def addNewGame(self):
		newData = self.getGameDataFromUser(checkForExistingGame = True, checkEmptyTitle = True)
		game = Game()
		game.setData(newData)
		self.gamesData.append(game)


	def checkIfBoardNameExist(self,titleName):
		for game in self.gamesData:
			if (game.gameData['Title'] == titleName):
				return True
		return False


	def getGameDataFromUser(self,checkForExistingGame = False, checkEmptyTitle = False):
		inputedData = {}
		for key, type in self.gameInfo.items():
			correctType = False
			while(correctType == False):
				value = self.rlinput(key+"("+type+") : ", "")
				if (value != ""):
					if (type == "number"):
						if (value.isnumeric() == False):
							print("Numeric data or empty required for this field.")
						else:
							correctType = True
					else:
						correctType = True
				else:
					correctType = True
				if (key == "Title" and checkForExistingGame):
					if (self.checkIfBoardNameExist(value)):
						print("The title already exist.")
						correctType = False

				if (key == "Title" and value == "" and checkEmptyTitle):
						print("The title can not be empty.")
						correctType = False

				inputedData[key] = value
		return inputedData


	def setGameData(self, gameIndex, gameData):
		self.gamesData[gameIndex].gameData = gameData


	def deleteGameDataRecord(self,gameToDelete):
		if (gameToDelete >= len(self.gamesData)):
			print("This game does not exist.")
			exit(0)
		del self.gamesData[gameToDelete]


	def editGameData(self, gameIndex):
		dataToEdit = self.gamesData[gameIndex]
		inputedData = {}
		for key, type in self.gameInfo.items():
			correctType = False
			while(correctType == False):
				value = self.rlinput(key+"("+type+") : ", dataToEdit.gameData[key])
				if (value != ""):
					if (type == "number"):
						if (value.isnumeric() == False):
							print("Numeric data or empty required for this field.")
						else:
							correctType = True
					else:
						correctType = True
				else:
					correctType = True
				if (key == "Title" and value == ""):
					print("The title can not be empty.")
					correctType = False
				inputedData[key] = value
		return inputedData


	def listFilteredGameData(self,filterData):
		for game in self.gamesData:
			filterData2 = {}
			gameData2 = {}
			for key, data in game.gameData.items(): # Create two arrays that contains the same keys,data so they can be compared. Only data that was entered into filterData should be used to compare.
				if (filterData[key] != ""):
					filterData2[key] = filterData[key]
					gameData2[key] = game.gameData[key]

			if (filterData2 == gameData2):
				game.printData()
				print("--------------")


	def sortWithRating(self):
		self.gamesData.sort(key=lambda x: x.getRating(), reverse=True)


#************************************************************************************************

if (len(sys.argv) <= 1):
	print('''
Arguments for this program : 
list		(List all saved board games)
list filter	(List all saved board games that equals to the inputed filter)
list sorted	(List all saved board games in order of rating)
new		(Input a new board game and save it)
delete x	(Delete game where 'x' is the number shown in list)
edit x		(Edit game where 'x' is the number shown in list)
''')
	exit(0)

games = BoardGames()
games.loadGamesData()

if (sys.argv[1] == "edit"):
	if (len(sys.argv) != 3):
		print("Argument for edit is missing or too many (should be one and a number).")
		exit(0)

	if (sys.argv[2].isnumeric() == False):
		print("Argument for edit should be a number.")
		exit(0)

	print("Please enter new data for game #",sys.argv[2])
	gameIndexToEdit = int(sys.argv[2]) - 1
	newData = games.editGameData(gameIndexToEdit)
	games.setGameData(gameIndexToEdit, newData)
	games.saveGameData()
	exit(0)


if (sys.argv[1] == "delete"):
	if (len(sys.argv) != 3):
		print("Argument for delete is missing or too many (should be one and a number).")
		exit(0)

	if (sys.argv[2].isnumeric() == False):
		print("Argument for delete should be a number.")
		exit(0)

	gameToDelete = int(sys.argv[2]) - 1
	games.deleteGameDataRecord(gameToDelete)
	games.saveGameData()
	exit(0)


if (sys.argv[1] == "list"):
	if (len(sys.argv) == 3 and sys.argv[2] == "filter"):
		filterData = games.getGameDataFromUser()
		games.listFilteredGameData(filterData)
		exit(0)

	if (len(sys.argv) == 3 and sys.argv[2] == "sorted"):
		games.sortWithRating()

	games.listGameData()
	exit(0)


if (sys.argv[1] == "new"):
	games.addNewGame()
	games.saveGameData()
	exit(0)
