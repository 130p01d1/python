#!/usr/bin/python3

#************************************************************************************************
 							# IMPORT MODULES
#************************************************************************************************

import os.path
import pickle
import sys

readLineFound = True
try:
	import readline
except ModuleNotFoundError:
	readLineFound = False


#************************************************************************************************
 							# CLASSES
#************************************************************************************************

class Game:
	"""Stores data about a SINGLE game."""
	def __init__(self):
		pass


	def setData(self, gameData):
		"""Insert game data into 'self.gameData' from parameters 'gameData'."""
		self.gameData = gameData


	def printData(self):
		"""Loop through each field in 'self.gameData' (Title ...) and print the key and value."""
		for key, data in self.gameData.items():
			print(key, " : ", data)

class BoardGames:
	"""Manages games (functions / methods) for loading user data, loading and saving games, etc."""
	gameInfo = {
		"Title": "string",
		"NumberOfPlayers": "number",
		"GameTime": "number",
		"RecommendedAge": "number",
		"Rating": "number"
	}

	fileName = "gamesdata.dat"


	def __init__(self):
		"""Initiate an empty list to which we will append / add game items."""
		self.gamesData = []
	
	
	def rlinput(self,prompt, prefill=''):
		"""This function is from https://stackoverflow.com/questions/5403138/how-to-set-a-default-editable-string-for-raw-input/20351345"""
		readline.set_startup_hook(lambda: readline.insert_text(prefill))
		try:
			return input(prompt)
		finally:
			readline.set_startup_hook()


	def loadGamesData(self):
		"""Checks if file exist, if yes: open and load the data saved to list 'gamesData' (which is a list of Games)."""
		if os.path.isfile(self.fileName):
			file = open(self.fileName, 'rb')
			self.gamesData = pickle.load(file)
			file.close()
		else:
			print("File '"+self.fileName+"' not found. This is normal when you start the program for the first time and has not entered any data.")


	def saveGameData(self):
		"""Open, save ('picke' module is used to save whats in 'self.gamesData' and close file."""
		file = open(self.fileName, 'wb')
		pickle.dump(self.gamesData, file)
		file.close()


	def listGameData(self, showGameNumber = True):
		"""Method for printing the games that are in gamesData, either with which number they appear in the list or not."""
		gameNumber = 1
		for game in self.gamesData:
			if (showGameNumber):
				print("Game #", gameNumber)
			game.printData()
			gameNumber += 1
			print("--------------")


	def addNewGame(self):
		"""Create a new game object from user input and save to list 'games.Data'."""
		newData = self.getGameDataFromUser(checkForExistingGame = True, checkEmptyTitle = True)
		game = Game()
		game.setData(newData)
		self.gamesData.append(game)


	def checkIfBoardNameExist(self,titleName):
		"""If Title == 'titleName' in list 'gamesData' return 'True' else 'False'."""
		for game in self.gamesData:
			if (game.gameData['Title'] == titleName):
				return True
		return False


	def getGameDataFromUser(self,checkForExistingGame = False, checkEmptyTitle = False, gameIndexToEdit = None):
		"""Used to handle rules regarding input data from user"""
		if (gameIndexToEdit != None):
			dataToEdit = self.gamesData[gameIndexToEdit]
		inputedData = {}
		for key, type in self.gameInfo.items():
			correctType = False
			while(correctType == False):
				if (readLineFound and gameIndexToEdit != None):
					value = self.rlinput(key+"("+type+") : ", dataToEdit.gameData[key])
				else:
					value = input(key+"("+type+") : ")
				if (value != ""):
					if (type == "number"):
						if (value.isnumeric() == False):
							print("Numeric data or empty required for this field.")
						else:
							correctType = True
					else:
						correctType = True
				else:
					correctType = True

				if (key == "Title" and checkForExistingGame):
					if (self.checkIfBoardNameExist(value)):
						print("The title already exist.")
						correctType = False

				if (key == "Title" and value == "" and checkEmptyTitle):
						print("The title can not be empty.")
						correctType = False

				inputedData[key] = value
		return inputedData


	def setGameData(self, gameIndex, gameData):
		"""Replace the gameData from the list gameData [gameIndex] with gameData."""
		self.gamesData[gameIndex].gameData = gameData


	def deleteGameDataRecord(self,gameToDelete):
		"""Check if the data we are trying to delete exists."""
		if (gameToDelete >= len(self.gamesData)):
			print("This game does not exist.")
			exit(0)
		del self.gamesData[gameToDelete]


	def listFilteredGameData(self,filterData):
		"""List filtered games in 'gameData'."""
		for game in self.gamesData:
			filterData2 = {}
			gameData2 = {}
			for key, data in game.gameData.items():
				if (filterData[key] != ""):
					filterData2[key] = filterData[key]
					gameData2[key] = game.gameData[key]

			if (filterData2 == gameData2):
				game.printData()
				print("--------------")


	def sortWithRating(self):
		"""List games with highest rating first. """
		self.gamesData.sort(key=lambda x: int(x.gameData["Rating"]), reverse=True)

#************************************************************************************************
 							# 'MENU'
#************************************************************************************************

if (len(sys.argv) <= 1):
	print('''
Arguments for this program : 
list		(List all saved board games)
list filter	(List all saved board games that equals to the inputed filter)
list sorted	(List all saved board games in order of rating)
new		(Input a new board game and save it)
delete x	(Delete game where 'x' is the number shown in list)
edit x		(Edit game where 'x' is the number shown in list)
''')
	exit(0)

games = BoardGames()
games.loadGamesData()

if (sys.argv[1] == "edit"):
	if (len(sys.argv) != 3):
		print("Argument for edit is missing or too many (should be one and a number).")
		exit(0)

	if (sys.argv[2].isnumeric() == False):
		print("Argument for edit should be a number.")
		exit(0)

	print("Please enter new data for game #",sys.argv[2])
	gameIndexToEdit = int(sys.argv[2]) - 1 
	newData = games.getGameDataFromUser(checkForExistingGame = False, checkEmptyTitle = True, gameIndexToEdit = gameIndexToEdit)

	games.setGameData(gameIndexToEdit, newData)
	games.saveGameData()
	exit(0)


if (sys.argv[1] == "delete"):
	if (len(sys.argv) != 3):
		print("Argument for delete is missing or too many (should be one and a number).")
		exit(0)

	if (sys.argv[2].isnumeric() == False):
		print("Argument for delete should be a number.")
		exit(0)

	gameToDelete = int(sys.argv[2]) - 1
	games.deleteGameDataRecord(gameToDelete)
	games.saveGameData()
	exit(0)


if (sys.argv[1] == "list"):
	if (len(sys.argv) == 3 and sys.argv[2] == "filter"):
		filterData = games.getGameDataFromUser()
		games.listFilteredGameData(filterData)
		exit(0)

	if (len(sys.argv) == 3 and sys.argv[2] == "sorted"):
		games.sortWithRating()
		games.listGameData(showGameNumber=False)
		exit(0)

	games.listGameData()
	exit(0)


if (sys.argv[1] == "new"):
	games.addNewGame()
	games.saveGameData()
	exit(0)
