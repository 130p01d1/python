#!/usr/bin/python3

#************************************************************************************************
 							# IMPORT MODULES
#************************************************************************************************

import os.path				# Importera funktioner för att exempelvis kolla om en fil existerar, används i loadGamesData.
import pickle				# Importera pickle så man lätt kan spara ner 'lists' och 'objects' för att sen kunna ladda dessa.
import sys

readLineFound = True		# Sätt readLineFound till 'True'.
try:
	import readline			# Importera readline.
except ModuleNotFoundError:
	readLineFound = False	# Om modulen readline inte kan hittas så sätts readLineFound till False så vi i programmet vet att vi inte kan använda readline.


#************************************************************************************************
 							# CLASSES
#************************************************************************************************

class Game:					# Definiera klassen Game som används för att lagra information om ETT spel.
	"""Stores data about a SINGLE game."""
	def __init__(self):
		pass


	def setData(self, gameData):	# Sätt ett spels data in i self.gameData från parameterns gameData.
		"""Insert game data into 'self.gameData' from parameters 'gameData'."""
		self.gameData = gameData


	def printData(self):
		"""Loop through each field in 'self.gameData' (Title ...) and print the key and value."""
		for key, data in self.gameData.items():		# Loopa igenom varje fält som finns i self.gameData (Title...) och skriv ut key och värdet.
			print(key, " : ", data)

class BoardGames:			# Definiera klassen BoardGames där vi har funktioner/metoder för att läsa in användardata, ladda och spara spel osv.
	"""Manages games (functions / methods) for loading user data, loading and saving games, etc."""
	gameInfo = {			# Skapa ett dictionary med de fält som ett spel ska innehålla och vad för slags data vi tillåter.
		"Title": "string",
		"NumberOfPlayers": "number",
		"GameTime": "number",
		"RecommendedAge": "number",
		"Rating": "number"
	}

	fileName = "gamesdata.dat"	# Filnamn för save/load.


	def __init__(self):
		"""Initiate an empty list to which we will append / add game items."""
		self.gamesData = []
	
	
	def rlinput(self,prompt, prefill=''):
		"""This function is from https://stackoverflow.com/questions/5403138/how-to-set-a-default-editable-string-for-raw-input/20351345"""
		readline.set_startup_hook(lambda: readline.insert_text(prefill))
		try:
			return input(prompt)
		finally:
			readline.set_startup_hook()


	def loadGamesData(self):
		"""Checks if file exist, if yes: open and load the data saved to list 'gamesData' (which is a list of Games)."""
		if os.path.isfile(self.fileName):		# Finns filen
			file = open(self.fileName, 'rb')	# Ja, öppna den
			self.gamesData = pickle.load(file)	# samt ladda in datan som är sparad till list gamesData (som är en list av Games)
			file.close()
		else:
			print("File '"+self.fileName+"' not found. This is normal when you start the program for the first time and has not entered any data.") # Om filen inte existerar brättar vi det för användaren.


	def saveGameData(self):
		"""Open, save ('picke' module is used to save whats in 'self.gamesData' and close file."""
		file = open(self.fileName, 'wb')		# Öppna fil att spara (w = write, b = binary)
		pickle.dump(self.gamesData, file)		# Låt pickle spara det som finns i self.gamesData
		file.close()							# Stäng filen.


	def listGameData(self, showGameNumber = True):
		"""Method for printing the games that are in gamesData, either with which number they appear in the list or not."""
		gameNumber = 1
		for game in self.gamesData:				# Loopa igenom alla spel
			if (showGameNumber):				# Om vi ska visa vilket nummer de förekommer i list...
				print("Game #", gameNumber)		# ...så skriver vi det.
			game.printData()					# Kallar på spelobjektets metod för att skriva ut sin data.
			gameNumber += 1
			print("--------------")


	def addNewGame(self):
		"""Create a new game object from user input and save to list 'games.Data'."""
		newData = self.getGameDataFromUser(checkForExistingGame = True, checkEmptyTitle = True) # Läs in input från användaren. Vi vill här kolla om speltitel redan finns samt se till att ingen tom titel tillåts.
		game = Game() 							# Skapa ett nytt spelobjekt.
		game.setData(newData)					# Sätt data i spelobjektet till det vi läste in från användaren.
		self.gamesData.append(game)				# Spara objektet i listan av spel.


	def checkIfBoardNameExist(self,titleName):
		"""If Title == 'titleName' in list 'gamesData' return 'True' else 'False'."""
		for game in self.gamesData:						# Gå igenom alla spel i listan gamesData.
			if (game.gameData['Title'] == titleName):	# Om Titeln överrentstämmer med titleName...
				return True								# ...så returnerar vi True, det finns alltså en befintligt spel med titeln = titleName i listan över spel (gamesData)
		return False									# Annars returnerar vi False, det finns då inget spel som heter samma.


	def getGameDataFromUser(self,checkForExistingGame = False, checkEmptyTitle = False, gameIndexToEdit = None):
		"""Used to handle rules regarding input data from user"""
		if (gameIndexToEdit != None):							# Om metoden kallas för att editera ett befintligt spel så ska vi...
			dataToEdit = self.gamesData[gameIndexToEdit] 		# ...hämta data om detta spelen och lägga den informationen i dictionary 'dataToEdit'
		inputedData = {} 										# Här sparar vi undan den data som användaren skriver in för att sen returnera den till där en kallades från.
		for key, type in self.gameInfo.items(): 				# Vi ska hämta data för varje entry i 'gameInfo'.
			correctType = False
			while(correctType == False): 						# Så länge correctType är 'False' så ska vi loopa.
				if (readLineFound and gameIndexToEdit != None): # Om import readline lyckades samt om gameIndexToEdit innehåller ett värde (som är spelnumret)....
					value = self.rlinput(key+"("+type+") : ", dataToEdit.gameData[key]) # ...så skriver vi ut vad det är för fält vi ska hämta data SAMT datan (ex, Titel), som via rlinput för det förinskrivna editerbart.
				else:
					value = input(key+"("+type+") : ") 			# Annars så hämtar vi bara en sträng.
				if (value != ""):								# Om värdet INTE är tomt...
					if (type == "number"):						#... så kollar vi om det är ett nummer vi är ute efter.
						if (value.isnumeric() == False): 		# Kolla om det inskrivna inte är ett nummervärde
							print("Numeric data or empty required for this field.") # Skriv ut felmeddelande.
						else:
							correctType = True 					# Om det det var ett nummer så sätter vi cirrectType till 'True' och därmed kan hoppa ur whileloopen.,
					else:
						correctType = True 						# Om type inte ska vara ett nummer så godtar vi det värdet som skrevt in (för att kolla det vidare lite senare)
				else:
					correctType = True 							# Om värdet är tomt så godkänner vi det för att kolla det vidare senare.

				if (key == "Title" and checkForExistingGame): 	# Är det title vi precis skrivit in data för och checkForExistingGame är satt...
					if (self.checkIfBoardNameExist(value)):		# ... så ska vi kolla om speltitlen existerar, för vi tillåter inte flera spel med samma namn.
						print("The title already exist.")
						correctType = False						# Sätter correctType till False för att twinga whileloopen att gå en runda till för att eventuellt få ett godkänt värde.

				if (key == "Title" and value == "" and checkEmptyTitle): # Om checkEmptyTitle är satt till True så ska vi kolla om Title är tomt, om vi ska hämta värden för en filtrering får Title vara tom, men inte vid nyinskrivning av data eller editering av befintligt spel.
						print("The title can not be empty.")
						correctType = False 					# Sätter correctType till False för att twinga whileloopen att gå en runda till för att eventuellt få ett godkänt värde.

				inputedData[key] = value 						# Lägg till datan i dictionary 'inputedData' och gå vidare i forloopen (om det nu inte var sista vi skrev in)
		return inputedData 										# returnera all data vi skrivit in.


	def setGameData(self, gameIndex, gameData):
		"""Replace the gameData from the list gameData [gameIndex] with gameData."""
		self.gamesData[gameIndex].gameData = gameData


	def deleteGameDataRecord(self,gameToDelete):
		"""Check if the data we are trying to delete exists."""
		if (gameToDelete >= len(self.gamesData)):
			print("This game does not exist.")
			exit(0)
		del self.gamesData[gameToDelete] # Ta bort.


	def listFilteredGameData(self,filterData):
		"""List filtered games in 'gameData'."""
		for game in self.gamesData: # Gå igenom alla spel som finns i gamesData
			filterData2 = {} # Skapa tomt dict
			gameData2 = {} # Skapa tomt dict
			for key, data in game.gameData.items(): # Gå igenom varje fält i det spel som finns i dess gameData.
				if (filterData[key] != ""):	# Om fältet inte är tomt i filterData...
					filterData2[key] = filterData[key]	# ...så lägger vi till den i filterData2...
					gameData2[key] = game.gameData[key]	# ...och gameData2.

			if (filterData2 == gameData2): # Om de två olika list är likadana så vet vi att de filten vi lagt in i filterData2 och gameData2 är samma och därför ska det spelet visas.
				game.printData()	# Visa spelet.
				print("--------------")


	def sortWithRating(self):
		"""List games with highest rating first. """
		self.gamesData.sort(key=lambda x: int(x.gameData["Rating"]), reverse=True) # Gå igenom varje spelobject som är sparat i gamesData, ge sorteringsrutinen tillgång till fältet Ratings för att det är det fältet den ska sortera på, sen anger vi att vi vill få det sorterad i från störtsta till minsta.

#************************************************************************************************
 							# 'MENU'
#************************************************************************************************

if (len(sys.argv) <= 1): # Om antal argument är mindre eller lika med 1 så skriver vi ut hur programmet ska användas.
	print('''
Arguments for this program : 
list		(List all saved board games)
list filter	(List all saved board games that equals to the inputed filter)
list sorted	(List all saved board games in order of rating)
new		(Input a new board game and save it)
delete x	(Delete game where 'x' is the number shown in list)
edit x		(Edit game where 'x' is the number shown in list)
''')
	exit(0)

games = BoardGames()			# Skapa objekt games från klassen BoardGames.
games.loadGamesData()			# Ladda in datafilen, om den finns.

if (sys.argv[1] == "edit"):		# If the first argument to the program is "edit..."
	if (len(sys.argv) != 3):	# ...så kollar vi om antal argument är tillräckligt för den funktionalitet vi är ute efter.
		print("Argument for edit is missing or too many (should be one and a number).")
		exit(0)

	if (sys.argv[2].isnumeric() == False):					# Är andra argumentet inte ett nummer?
		print("Argument for edit should be a number.")		# Skriv ut felmeddelande...
		exit(0)												# Och avsluta programmet.

	print("Please enter new data for game #",sys.argv[2])
	gameIndexToEdit = int(sys.argv[2]) - 1 
	newData = games.getGameDataFromUser(checkForExistingGame = False, checkEmptyTitle = True, gameIndexToEdit = gameIndexToEdit)	# Hämta speldata från användaren.

	games.setGameData(gameIndexToEdit, newData)				# Spara den nya speldatan i gamesData via setGameDatametoden.
	games.saveGameData()									# Spara datan till fil.
	exit(0)


if (sys.argv[1] == "delete"):
	if (len(sys.argv) != 3):
		print("Argument for delete is missing or too many (should be one and a number).")
		exit(0)

	if (sys.argv[2].isnumeric() == False):
		print("Argument for delete should be a number.")
		exit(0)

	gameToDelete = int(sys.argv[2]) - 1
	games.deleteGameDataRecord(gameToDelete)			# Ta bort gamesData[gameToDelete]
	games.saveGameData()								# Och spara gamesData till fil.
	exit(0)


if (sys.argv[1] == "list"):
	if (len(sys.argv) == 3 and sys.argv[2] == "filter"):
		filterData = games.getGameDataFromUser()		# Hämta filterdata från användaren.
		games.listFilteredGameData(filterData)			# Skriv ut matchande data.
		exit(0)

	if (len(sys.argv) == 3 and sys.argv[2] == "sorted"):
		games.sortWithRating() 							# Sortera datan som finns i games.gamesData...
		games.listGameData(showGameNumber=False)		# ...och skriv sedan ut resultatet UTAN spelnummer.
		exit(0)

	games.listGameData()								# Om vi inte valt filter eller sorted så skriver vi ut spelen som vanligt.
	exit(0)


if (sys.argv[1] == "new"):
	games.addNewGame()									# Ge användaren skriva in data till ett nytt spel.
	games.saveGameData()								# Spara spelen till fil.
	exit(0)
