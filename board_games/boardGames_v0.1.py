#!/usr/bin/python3

import os.path
import json
import sys
import readline

# Fields and their required types, but still takes "" as a value, except for Title.
gameInfo = {
	"Title": "string",
	"NumberOfPlayers": "number",
	"GameTime": "number",
	"RecommendedAge": "number",
	"Rating": "number"
}

fileName = "gamedata.json"
gameData = []

#************************************
# Functions
#************************************

def rlinput(prompt, prefill=''): # This function is from https://stackoverflow.com/questions/5403138/how-to-set-a-default-editable-string-for-raw-input/20351345
	readline.set_startup_hook(lambda: readline.insert_text(prefill))
	try:
		return input(prompt)
	finally:
		readline.set_startup_hook()


# Load JSON-file into gameData struncture.
def loadGameData():
	global gameData
	if os.path.isfile(fileName):
		with open(fileName) as f:
			gameData = json.load(f)
	else:
		print(fileName,"not found. This is normal when you start the program for the first time and has not entered any data.")


# Save the structure gameData into a JSON-file.
def saveGameData():
	with open(fileName, 'w') as f:
		json.dump(gameData, f)	


# List games in the struncture gameData.
def listGameData():
	gameNumber = 1
	for game in gameData:
		print("Game #",gameNumber)
		for key, data in game.items():
			print(key, " : ", data)
		gameNumber += 1
		print("--------------")


# List games that has the same data as the array filterData.
def listFilteredGameData(filterData):
	gameNumber = 1
	for game in gameData:
		filterData2 = {}
		gameData2 = {}
		showGame = False
		for key, data in game.items(): # Compose two arrays that contains the same keys,data so they can be compared. Only data that was entered into filterData should be used to compare.
			if (filterData[key] != ""):
				filterData2[key] = filterData[key]
				gameData2[key] = game[key]

		if (filterData2 == gameData2):
			showGame = True

		if(showGame):
			print("Game #", gameNumber)
			for key, data in game.items():
				print(key, " : ", data)
			print("--------------")
		gameNumber += 1


# Check if a title already exist in the gameData structure.
def checkIfBoardNameExist(titleName):
	for game in gameData:
		if (game['Title'] == titleName):
			return True
	return False


# This function is used when the user want to insert new data or when you want to get what a user want to get data to filter the listing.
def getGameDataFromUser(checkForExistingGame = False, checkEmptyTitle = False):
	inputedData = {}
	for key, type in gameInfo.items():
		correctType = False
		while(correctType == False):
			value = rlinput(key+"("+type+") : ", "")
			if (value != ""):
				if (type == "number"):
					if (value.isnumeric() == False):
						print("Numeric data or clear required for this field.")
					else:
						correctType = True
				else:
					correctType = True
			else:
				correctType = True
			if (key == "Title" and checkForExistingGame):
				if (checkIfBoardNameExist(value)):
					print("The title already exist.")
					correctType = False

			if (key == "Title" and value == "" and checkEmptyTitle):
					print("The title can not be empty.")
					correctType = False

			inputedData[key] = value
	return inputedData


# This function get user input for editing a record. It contains many elements from the above but for *simplicity* we use two functions.
def editGameData(dataToEdit):
	inputedData = {}
	for key, type in gameInfo.items():
		correctType = False
		while(correctType == False):
			value = rlinput(key+"("+type+") : ", dataToEdit[key])
			if (value != ""):
				if (type == "number"):
					if (value.isnumeric() == False):
						print("Numeric data or clear required for this field.")
					else:
						correctType = True
				else:
					correctType = True
			else:
				correctType = True
			if (key == "Title" and value == ""):
				print("The title can not be empty.")
				correctType = False
			inputedData[key] = value
	return inputedData


def deleteGameDataRecord(idx):
	gameToDelete = idx
	if (gameToDelete >= len(gameData)):
		print("This game does not exist.")
		exit(0)
	del gameData[gameToDelete]


def getRating(game):
	if (game.get('Rating') != ""):
		return int(game.get('Rating'))
	else:
		return 0

#************************************



if (len(sys.argv) <= 1):
	print('''
Arguments for this program : 
list		(List all saved board games)
list filter	(List all saved board games that equals to the inputed filter)
list sorted	(List all saved board games in order of rating)
new		(Input a new board game and save it)
delete x	(Delete game where 'x' is the number shown in list)
edit x		(Edit game where 'x' is the number shown in list)
''')
	exit(0)


loadGameData()


if (sys.argv[1] == "edit"):
	if (len(sys.argv) != 3):
		print("Argument for edit is missing or too many (should be one and a number).")
		exit(0)

	if (sys.argv[2].isnumeric() == False):
		print("Argument for edit should be a number.")
		exit(0)

	print("Please enter new data for game #",sys.argv[2])
	gameToEdit = int(sys.argv[2]) - 1
	newData = editGameData(gameData[gameToEdit])
	gameData[gameToEdit] = newData
	saveGameData()


if (sys.argv[1] == "delete"):
	if (len(sys.argv) != 3):
		print("Argument for delete is missing or too many (should be one and a number).")
		exit(0)

	if (sys.argv[2].isnumeric() == False):
		print("Argument for delete should be a number.")
		exit(0)

	gameToDelete = int(sys.argv[2]) - 1
	deleteGameDataRecord(gameToDelete)
	saveGameData()
	exit(0)

if (sys.argv[1] == "list"):
	if (len(sys.argv) == 3 and sys.argv[2] == "filter"):
		filterData = getGameDataFromUser()
		listFilteredGameData(filterData)
		exit(0)

	if (len(sys.argv) == 3 and sys.argv[2] == "sorted"):
		gameData.sort(key=getRating,reverse=True)

	listGameData()
	exit(0)

if (sys.argv[1] == "new"):
	newData = getGameDataFromUser(True, True)
	gameData.append(newData)
	saveGameData()
	exit(0)
